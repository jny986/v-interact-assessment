export default () => ({
    term: '',
    alertMessage: '',
    alertType: '',
    alertDisplay: false,
    search: false,
    pageSize: 10,
    title: ''
})