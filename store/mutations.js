export default ({
    updateTerm(state, term) {
        state.term = term;
    },
    setAlert(state, alert) {
        state.alertMessage = alert.message;
        state.alertType = alert.type;
        state.alertDisplay = alert.display;
    },
    setSearch(state, value) {
        state.search = value;
    },
    setPageSize(state, size) {
        state.pageSize = size;
    },
    setTitle(state, title) {
        state.title = title;
    }
})